package com.hazelcast.controller;

import com.hazelcast.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by harkesh on 27/1/17.
 */
@RestController
@RequestMapping(value = "api")
public class ApiController {

  @Autowired
  UserService userService;

  @GetMapping(value = "user/{id}")
  public String readUserData(@PathVariable Long id){
    return userService.findUserById(id).getFirstName();
  }

  @GetMapping(value = "add/user/{count}")
  public String saveUserData(@PathVariable int count){
    userService.addUser(count);
    return "done";
  }

}
