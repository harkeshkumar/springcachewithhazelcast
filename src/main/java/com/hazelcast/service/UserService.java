package com.hazelcast.service;

import com.hazelcast.entity.User;

/**
 * Created by harkesh on 29/1/17.
 */
public interface UserService {

  User findUserById(Long id);

  void addUser(int count);
}
