package com.hazelcast.service.impl;

import com.hazelcast.entity.User;
import com.hazelcast.repository.UserRepository;
import com.hazelcast.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Created by harkesh on 29/1/17.
 */
@Service
public class UserServiceImpl implements UserService{

  @Autowired
  UserRepository userRepository;

  @Override
  @Cacheable(value = "userDataMap")
  public User findUserById(Long id) {
    System.out.println("Getting data from DB");
    return userRepository.findOne(id);
  }

  @Override
  public void addUser(int count) {
    User user;
    while (count > 0){
      user = new User("first"+count, "last"+count, "address"+count);
      userRepository.save(user);
      count--;
    }
  }
}
