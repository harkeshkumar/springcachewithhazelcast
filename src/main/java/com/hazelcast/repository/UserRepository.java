package com.hazelcast.repository;

import com.hazelcast.entity.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by harkesh on 29/1/17.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long>{
}
