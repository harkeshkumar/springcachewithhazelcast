package com.hazelcast.config;

import com.hazelcast.spring.cache.HazelcastCacheManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by harkesh on 8/11/16.
 */
@Configuration
@EnableCaching
public class SpringCacheCfg {

  @Autowired
  private HazelcastClientInstance hazelcastClientInstance;

  @Bean
  public CacheManager cacheManager() {
    CacheManager cacheManager = new HazelcastCacheManager(hazelcastClientInstance.getInstance());
    return cacheManager;
  }
}
