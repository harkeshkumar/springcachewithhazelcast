package com.hazelcast.config;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.core.HazelcastInstance;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by harkesh on 27/1/17.
 */
@Component
public class HazelcastClientInstance {

  @Value("${members}")
  private String members;
  private String[] hosts;
  @Value("${group.user}")
  private String user;
  @Value("${group.password}")
  private String password;

  private HazelcastInstance hazelcastInstance;

  @PostConstruct
  public void init() {
    hosts = members.split(",");
    hazelcastInstance = createInstance();
  }

  public HazelcastInstance getInstance() {
    if (hazelcastInstance == null) {
      return createInstance();
    } else {
      return hazelcastInstance;
    }
  }

  private HazelcastInstance createInstance() {
    return HazelcastClient.newHazelcastClient(configuration());
  }

  private ClientConfig configuration() {
    ClientConfig clientConfig = new ClientConfig();
    clientConfig.getGroupConfig()
            .setName(user)
            .setPassword(password);
    ClientNetworkConfig networkConfig = clientConfig.getNetworkConfig();
    for (String host : hosts) {
      networkConfig.addAddress(host);
    }
    networkConfig.setConnectionAttemptLimit(10) // by default 2
            .setConnectionAttemptPeriod(5000) // by default 3000 ms
            .setConnectionTimeout(7000); // by default 5000 ms
    return clientConfig;
  }
}

